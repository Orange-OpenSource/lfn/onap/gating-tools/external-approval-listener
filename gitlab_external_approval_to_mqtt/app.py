#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from configparser import ConfigParser
from json import dumps, loads, JSONDecodeError
from logging import Handler
from os import getenv
from pathlib import Path
from sys import stdout
from typing import Union, Tuple
from collections.abc import Mapping

from flask import Flask
from flask import request
from loguru import logger
from paho.mqtt import publish

class PushMQTT():  # pylint: disable=too-few-public-methods
    def __init__(self, config: Mapping[str, Union[str, int, Mapping[str, str]]]) -> None:
        logger.debug("creating a mqqt client with current values:")
        logger.debug("  * hostname: {}", config['hostname'])
        logger.debug("  * topic: {}", config['topic'])
        logger.debug("  * port: {}", config['port'])
        logger.debug("  * keepalive: {}", config['keepalive'])
        logger.debug("  * qos: {}", config['qos'])
        logger.debug("  * auth: {}", config['auth'])

        self.hostname = config['hostname']
        self.topic = config['topic']
        self.port = config['port']
        self.keepalive = config['keepalive']
        self.auth = config['auth']
        self.qos = config['qos']

    def publish_single(self, msg: str) -> None:
        logger.debug("Sending message in topic {}", self.topic)
        publish.single(self.topic,
                       msg,
                       hostname=self.hostname,
                       port=self.port,
                       keepalive=self.keepalive,
                       auth=self.auth,
                       qos=self.qos,
                       )

app = Flask(__name__)
MQTT_QUEUE: Union[None, PushMQTT] = None
JOB_NAME = ''
TRIGGER_LABELS = []


def _get_config() -> Union[ConfigParser, None]:
    """Read the config file and return the configuration.

    :returns: configparser.ConfigParser object

    :seealso: https://docs.python.org/3/library/configparser.html
    """
    config_file = getenv('CONFIG_FILE')
    config = None
    if config_file:
        logger.info('retrieving {} configuration file', config_file)
        config = ConfigParser()
        config.optionxform = lambda optionstr: optionstr
        config.read(Path(config_file))
    return config


def _parse_config(
    config: Union[ConfigParser, None]
) -> Mapping[str, Mapping[str, Union[str, int, Mapping[str, str]]]]:
    parsed_config = {}
    if config:
        labels = [
        v for v in config.get('gitlab', 'labels').split('\n') if v != ''  # noqa: WPS111
        ]
        # Configure auth
        auth = None
        mqtt_username = config.get('mqtt', 'username', fallback=None)
        mqtt_password = config.get('mqtt', 'password', fallback=None)
        if mqtt_username:
            auth = {'username': mqtt_username}
            if mqtt_password:
                auth['password'] = mqtt_password

        parsed_config = {
            'gitlab': {
                'job_name': config.get('gitlab', 'job'),
                'labels': labels,
            },
            'mqtt': {
                    'qos': config.getint('mqtt', 'qos', fallback=0),
                    'port': config.getint(
                        'mqtt',
                        'port',
                        fallback=1883,  # noqa: WPS432 Found magic number: 1883
                    ),
                    'keepalive': config.getint('mqtt', 'keepalive', fallback=60),
                    'hostname': config.get('mqtt', 'hostname'),
                    'auth': auth,
                    'topic': config.get('mqtt', 'topic'),
            },
        }
        logger.debug("config parsed: {}", config)
    return parsed_config


def handle_validation(payload: Mapping) -> bool:
    logger.debug("checking if we need to trigger a message to MQTT topic")
    try:
        attributes = payload["object_attributes"]
        changes = payload["changes"]
        changes_keys = list(changes.keys())
        if 'updated_at' in changes_keys:
            changes_keys.remove('updated_at')
        trigger_label_fired = False
        trigger_label_fired_name = ""
        if "labels" in changes:
            logger.debug(
                "labels have changed, checking if a trigger label has been set",
            )
            logger.debug(
                "will trigger if the following labels are new: {}",
                TRIGGER_LABELS,
            )
            previous_labels = []
            for label in changes["labels"]["previous"]:
                previous_labels.append(label["title"])
            logger.debug("previous labels: {}", previous_labels)
            current_labels = []
            for label in changes["labels"]["current"]:
                current_labels.append(label["title"])
            logger.debug("current labels: {}", current_labels)
            new_labels = []
            for label in current_labels:
                if label not in previous_labels:
                    new_labels.append(label)
            logger.debug("new labels: {}", new_labels)
            for label in new_labels:
                if label in TRIGGER_LABELS:
                    trigger_label_fired = True
                    trigger_label_fired_name = label
                    logger.debug(
                        "the added label {} is a trigger label, {}",
                        label,
                        "asking to send a message into MQTT",
                        )
                    break
        if len(changes_keys) == 0:
            logger.debug(
                "a new commit has been pushed, {}",
                "asking to send a message into MQTT",
                )
        if len(changes_keys) == 0 or trigger_label_fired:
            message = {
                'job': JOB_NAME,
                'pipeline_id': attributes["head_pipeline_id"],
                'source_project_id': attributes["source"]["id"],
                'target_project_id': attributes["target"]["id"],
                'merge_request_id': attributes["iid"],
                'last_commit_id': attributes["last_commit"]["id"],
                'label': trigger_label_fired_name,
                'project': attributes["target"]["path_with_namespace"],
                'branch': attributes['target_branch'],
            }
            if MQTT_QUEUE:
                MQTT_QUEUE.publish_single(dumps(message))
        return True
    except KeyError:
        logger.error("This is not a valid payload")
        return False


class InterceptHandler(Handler):
    def emit(self, record):
        # Retrieve context where the logging call occurred, this happens to be
        # in the 6th frame upward
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(record.levelno, record.getMessage())


@app.route('/healthz')
def health() -> str:
    return "OK"


@app.post("/validate")
def validate() -> Tuple[str, int]:
    data = request.get_data().decode("utf-8")
    logger.debug("data received: {}", data)
    try:
        payload = loads(data)
        if handle_validation(payload):
            return "OK", 200
        return 'bad JSON file', 400
    except JSONDecodeError:
        logger.error("This is not a valid JSON payload")
        return 'not a JSON file', 400


def start() -> None:
    logger.remove()
    logger.add(stdout, level='DEBUG')
    # this method is here to set the constants so we need global
    # pylint: disable=global-statement
    global MQTT_QUEUE
    global TRIGGER_LABELS
    global JOB_NAME
    # pylint: enable=global-statement
    config = _parse_config(_get_config())

    app.logger.addHandler(InterceptHandler())  # pylint: disable=no-member

    logger.debug("MQTT client creation")
    MQTT_QUEUE = PushMQTT(config['mqtt'])
    TRIGGER_LABELS = config['gitlab']['labels']
    JOB_NAME = config['gitlab']['job_name']

    app.run(host='0.0.0.0')


if __name__ == '__main__':
    start()
