# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

import paho.mqtt.publish
from gitlab_external_approval_to_mqtt import app


@mock.patch.object(paho.mqtt.publish, 'single')
def test_publish_msg_on_topic(publish_single_mock):
    config = {
      'qos': 2,
      'port': 1234,
      'keepalive': 30,
      'hostname': 'hostname',
      'auth': {'username': 'david'},
      'topic': 'topic'
      }
    app.mqttqueue = app.PushMQTT(config)
    app.mqttqueue.publish_single("message")
    publish_single_mock.assert_called_once_with(
        'topic',
        'message',
        hostname='hostname',
        port=1234,
        keepalive=30,
        auth={"username": "david"},
        qos=2,
    )
