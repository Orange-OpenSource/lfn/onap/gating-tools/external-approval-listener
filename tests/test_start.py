# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os

import flask
import mock

from gitlab_external_approval_to_mqtt import app


@mock.patch.dict(os.environ, {"CONFIG_FILE": "etc/gitlab-external-approval-to-mqtt.conf"})
@mock.patch.object(flask.Flask, 'run')
def test_start(flask_mock):
    app.start()
    assert flask_mock.called_once()
    assert app.MQTT_QUEUE.hostname == "localhost"
    assert app.MQTT_QUEUE.topic == "the_topic"
    assert app.JOB_NAME == "the_job"
    assert 'label_1' in app.TRIGGER_LABELS
    assert 'label_2' in app.TRIGGER_LABELS
