# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# this is how is working pytest...
# pylint: disable=redefined-outer-name
import pytest


@pytest.fixture()
def valid_config_mqtt_parsed():
    return {
        'qos': 2,
        'port': 1883,
        'keepalive': 60,
        'hostname': "localhost",
        'auth': {
            'username': 'username',
            'password': 'password'
        },
        'topic': 'the_topic',
    }


@pytest.fixture()
def valid_config_gitlab_parsed():
    return {
        'job_name': 'the_job',
        'labels': ['label_1', 'label_2'],
    }


@pytest.fixture()
def valid_config_parsed(valid_config_mqtt_parsed, valid_config_gitlab_parsed):
    return {
        'gitlab': valid_config_gitlab_parsed,
        'mqtt': valid_config_mqtt_parsed,
    }
