# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json
import mock

from gitlab_external_approval_to_mqtt import app


@mock.patch.object(app.PushMQTT, 'publish_single')
def test_handle_validation_bad_payload(publish_single_mock):
    payload = {}
    assert app.handle_validation(payload) is False
    publish_single_mock.assert_not_called()


@mock.patch.object(app.PushMQTT, 'publish_single')
def test_handle_validation_other_change(publish_single_mock, valid_config_mqtt_parsed):
    app.MQTT_QUEUE = app.PushMQTT(valid_config_mqtt_parsed)
    payload = {
        'object_attributes': {
            'head_pipeline_id': 123,
            'iid': 2,
            'target_branch': 'master',
            'source': {
                'id': 456,
            },
            'target': {
                'id': 789,
                'path_with_namespace': 'the/project',
            },
            'last_commit': {
                'id': 135,
            },
        },
        'changes': {
            'description': {
                'previous': 'old',
                'current': 'new',
            },
            'last_edited_at': {
                'previous': '2021-11-04 12:39:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
            'updated_at': {
                'previous': '2021-11-16 10:03:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
        }
    }
    assert app.handle_validation(payload) is True
    publish_single_mock.assert_not_called()


@mock.patch.object(app.PushMQTT, 'publish_single')
def test_handle_validation_new_commit(publish_single_mock, valid_config_mqtt_parsed):
    app.MQTT_QUEUE = app.PushMQTT(valid_config_mqtt_parsed)
    app.JOB_NAME = 'job'
    payload = {
        'object_attributes': {
            'head_pipeline_id': 123,
            'iid': 2,
            'target_branch': 'master',
            'source': {
                'id': 456,
            },
            'target': {
                'id': 789,
                'path_with_namespace': 'the/project',
            },
            'last_commit': {
                'id': 135,
            },
        },
        'changes': {
            'updated_at': {
                'previous': '2021-11-16 10:03:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
        }
    }
    assert app.handle_validation(payload) is True
    publish_single_mock.assert_called_once_with(json.dumps({
        'job': 'job',
        'pipeline_id': 123,
        'source_project_id': 456,
        'target_project_id': 789,
        'merge_request_id': 2,
        'last_commit_id': 135,
        'label': '',
        'project': 'the/project',
        'branch': 'master',
    }))


@mock.patch.object(app.PushMQTT, 'publish_single')
def test_handle_validation_label_change_no_trigger_1(publish_single_mock, valid_config_mqtt_parsed):
    app.MQTT_QUEUE = app.PushMQTT(valid_config_mqtt_parsed)
    app.TRIGGER_LABELS = ['label_1', 'label_2']
    payload = {
        'object_attributes': {
            'head_pipeline_id': 123,
            'iid': 2,
            'target_branch': 'master',
            'source': {
                'id': 456,
            },
            'target': {
                'id': 789,
                'path_with_namespace': 'the/project',
            },
            'last_commit': {
                'id': 135,
            },
        },
        'changes': {
            'labels': {
                'previous': [],
                'current': [
                    {'title': 'label_3'},
                ],
            },
            'last_edited_at': {
                'previous': '2021-11-04 12:39:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
        }
    }
    assert app.handle_validation(payload) is True
    publish_single_mock.assert_not_called()


@mock.patch.object(app.PushMQTT, 'publish_single')
def test_handle_validation_label_change_no_trigger_2(publish_single_mock, valid_config_mqtt_parsed):
    app.MQTT_QUEUE = app.PushMQTT(valid_config_mqtt_parsed)
    app.TRIGGER_LABELS = ['label_1', 'label_2']
    payload = {
        'object_attributes': {
            'head_pipeline_id': 123,
            'iid': 2,
            'target_branch': 'master',
            'source': {
                'id': 456,
            },
            'target': {
                'id': 789,
                'path_with_namespace': 'the/project',
            },
            'last_commit': {
                'id': 135,
            },
        },
        'changes': {
            'labels': {
                'previous': [
                    {'title': 'label_1'},
                ],
                'current': [
                    {'title': 'label_3'},
                    {'title': 'label_1'},
                ],
            },
            'last_edited_at': {
                'previous': '2021-11-04 12:39:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
            'updated_at': {
                'previous': '2021-11-16 10:03:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
        }
    }
    assert app.handle_validation(payload) is True
    publish_single_mock.assert_not_called()


@mock.patch.object(app.PushMQTT, 'publish_single')
def test_handle_validation_label_change_trigger(publish_single_mock, valid_config_mqtt_parsed):
    app.MQTT_QUEUE = app.PushMQTT(valid_config_mqtt_parsed)
    app.JOB_NAME = 'job'
    app.TRIGGER_LABELS = ['label_1', 'label_2']
    payload = {
        'object_attributes': {
            'head_pipeline_id': 123,
            'iid': 2,
            'target_branch': 'master',
            'source': {
                'id': 456,
            },
            'target': {
                'id': 789,
                'path_with_namespace': 'the/project',
            },
            'last_commit': {
                'id': 135,
            },
        },
        'changes': {
            'labels': {
                'previous': [
                    {'title': 'label_3'},
                ],
                'current': [
                    {'title': 'label_2'},
                    {'title': 'label_3'},
                ],
            },
            'updated_at': {
                'previous': '2021-11-16 10:03:40 UTC',
                'current': '2021-11-16 13:56:41 UTC',
            },
        }
    }
    assert app.handle_validation(payload) is True
    publish_single_mock.assert_called_once_with(json.dumps({
        'job': 'job',
        'pipeline_id': 123,
        'source_project_id': 456,
        'target_project_id': 789,
        'merge_request_id': 2,
        'last_commit_id': 135,
        'label': 'label_2',
        'project': 'the/project',
        'branch': 'master',
    }))
