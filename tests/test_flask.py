# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# this is how is working pytest...
# pylint: disable=redefined-outer-name
import mock
import pytest

from gitlab_external_approval_to_mqtt import app


@pytest.fixture
def client():
    app.app.config['TESTING'] = True
    app.app.config['SERVER_NAME'] = 'TEST'
    client = app.app.test_client()
    with app.app.app_context():
        pass
    app.app.app_context().push()
    yield client


def test_healthz(client):
    request = client.get('/healthz')
    assert request.status_code == 200
    assert b'OK' in request.data


def test_validate_bad_json(client):
    request = client.post(
        '/validate',
        data=dict(username="someone", other="variable"),
    )
    assert request.status_code == 400
    assert b'not a JSON file' in request.data


@mock.patch('gitlab_external_approval_to_mqtt.app.handle_validation')
def test_validate_false_from_handle_validation(handle_validation_mock, client):
    handle_validation_mock.return_value = False
    request = client.post(
        '/validate',
        data=b'{"a": "json", "file": true}',
    )
    assert request.status_code == 400
    assert b'bad JSON file' in request.data


@mock.patch('gitlab_external_approval_to_mqtt.app.handle_validation')
def test_validate_true_from_handle_validation(handle_validation_mock, client):
    handle_validation_mock.return_value = True
    request = client.post(
        '/validate',
        data=b'{"a": "json", "file": true}',
    )
    assert request.status_code == 200
    assert b'OK' in request.data
