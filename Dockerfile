FROM nexus3.onap.org:10001/onap/integration-python:9.1.0

LABEL name="gitlab_external_approval_to_mqtt" \
      version="v0.1.dev0" \
      architecture="x86_64"

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

ENV FLASK_APP=gitlab_external_approval_to_mqtt/app
ENV CONFIG_FILE=/app/etc/gitlab-external-approval-to-mqtt.conf

EXPOSE 5000/tcp

CMD [ "python3", "gitlab_external_approval_to_mqtt/app.py"]
